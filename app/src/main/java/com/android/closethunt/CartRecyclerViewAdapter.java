package com.android.closethunt;

import android.content.Context;

import android.content.Intent;
import android.media.Image;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.List;

/**
 * Created by dell on 03-Jul-17.
 */

public class CartRecyclerViewAdapter extends RecyclerView.Adapter<CartRecyclerViewAdapter.ViewHolder> {

    Context context;
    List<GetDataAdapter> getDataAdapter;
    StorageReference storageReference;
    String fileName;
    AppCompatTextView total;

    public CartRecyclerViewAdapter(List<GetDataAdapter> getDataAdapter, Context context, AppCompatTextView total) {

        super();
        this.getDataAdapter = getDataAdapter;
        this.context = context;
        this.total = total;
    }

    @Override
    public CartRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_card, parent, false);
        CartRecyclerViewAdapter.ViewHolder viewHolder = new CartRecyclerViewAdapter.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CartRecyclerViewAdapter.ViewHolder holder, final int position) {

        String amount = String.valueOf(((Integer.parseInt(getDataAdapter.get(position).getPrice()))*(getDataAdapter.get(position).getQty())));
        holder.Name.setText(getDataAdapter.get(position).getName());
        holder.Price.setText(amount);
        holder.Quantity.setText(String.valueOf(getDataAdapter.get(position).getQty()));
        holder.Size.setText(getDataAdapter.get(position).getSize());
        fileName = getDataAdapter.get(position).getFileName();
        storageReference = FirebaseStorage.getInstance().getReference().child(fileName);

        Glide.with(context)
                .using(new FirebaseImageLoader())
                .load(storageReference)
                .placeholder(android.R.drawable.progress_indeterminate_horizontal)
                .into(holder.pic);

        holder.appCompatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DBHelper dbHelper = new DBHelper(context);
                dbHelper.removeFromCart(getDataAdapter.get(position).getId(), getDataAdapter.get(position).getSize());
                getDataAdapter.remove(getDataAdapter.get(position));
                total.setText("INR. "+ String.valueOf(dbHelper.getTotalAmount()));
                Toast.makeText(context, "Item removed from cart!", Toast.LENGTH_LONG).show();
                notifyDataSetChanged();
            }
        });

        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DBHelper dbHelper = new DBHelper(context);
                getDataAdapter.get(position).setQty(Integer.parseInt(holder.Quantity.getText().toString()) + 1);
                dbHelper.updateData(getDataAdapter.get(position));
                total.setText("INR. "+ String.valueOf(dbHelper.getTotalAmount()));

                Toast.makeText(context, "Added Quantity", Toast.LENGTH_LONG).show();

                        notifyDataSetChanged();
            }
        });

        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Integer.parseInt(holder.Quantity.getText().toString())>1){
                    DBHelper dbHelper = new DBHelper(context);
                    getDataAdapter.get(position).setQty(Integer.parseInt(holder.Quantity.getText().toString()) - 1);
                    dbHelper.updateData(getDataAdapter.get(position));
                    total.setText("INR. "+ String.valueOf(dbHelper.getTotalAmount()));

                    Toast.makeText(context, "Reduced Quantity", Toast.LENGTH_LONG).show();
                            notifyDataSetChanged();
                } else{
                    Toast.makeText(context, "Select 1 or more items", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return getDataAdapter.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView Name;
        public TextView Price, Quantity, Size;
        public ImageView pic, appCompatButton;
        public AppCompatButton plus, minus;

        View view;

        public ViewHolder(View itemView) {

            super(itemView);
            view = itemView;
            Name = (TextView) itemView.findViewById(R.id.textView0);
            Price = (TextView) itemView.findViewById(R.id.textView1);
            Quantity = (TextView) itemView.findViewById(R.id.textView100);
            Size = (TextView) itemView.findViewById(R.id.size);
            appCompatButton = (ImageView) itemView.findViewById(R.id.cancel);
            pic = (ImageView) itemView.findViewById(R.id.pic);
            plus = (AppCompatButton) itemView.findViewById(R.id.plus);
            minus = (AppCompatButton) itemView.findViewById(R.id.minus);
        }


    }
}
