package com.android.closethunt;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.android.closethunt.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by dell on 09-Jun-17.
 */

public class Updatepassword_Frag extends Fragment {

    private TextInputLayout textInputLayoutPassword;
    private TextInputLayout textInputLayoutConfirmPassword;
    private TextInputEditText textInputEditTextConfirmPassword;
    private TextInputEditText textInputEditTextOldPassword;
    private TextInputLayout textInputLayoutOldPassword;
    private TextInputEditText textInputEditTextPassword;
    private Button appCompatButtonChange;
    InputValidation inputValidation;
    ProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_change_picture, container, false);
        inputValidation = new InputValidation(getActivity());
        textInputLayoutPassword = (TextInputLayout) view.findViewById(R.id.textInputLayoutPassword);
        textInputLayoutConfirmPassword = (TextInputLayout) view.findViewById(R.id.textInputLayoutConfirmPassword);
        textInputEditTextPassword = (TextInputEditText) view.findViewById(R.id.textInputEditTextPassword);
        textInputEditTextConfirmPassword = (TextInputEditText) view.findViewById(R.id.textInputEditTextConfirmPassword);
        textInputLayoutOldPassword = (TextInputLayout) view.findViewById(R.id.textInputLayoutOldPassword);
        textInputEditTextOldPassword = (TextInputEditText) view.findViewById(R.id.textInputEditTextOldPassword);
        appCompatButtonChange = (Button) view.findViewById(R.id.btn_reset_password);
        appCompatButtonChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!inputValidation.isInputEditTextFilled(textInputEditTextOldPassword, textInputLayoutOldPassword, "Enter Password")) {
                    return;
                }
                if (!inputValidation.isInputEditTextFilled(textInputEditTextPassword, textInputLayoutPassword, "Enter Password")) {
                    return;
                }
                if (inputValidation.isPasswordShort(textInputEditTextPassword, textInputLayoutPassword, "Password must be between 6 to 20 characters")) {
                    return;
                }
                if (!inputValidation.isInputEditTextMatches(textInputEditTextPassword, textInputEditTextConfirmPassword,
                        textInputLayoutConfirmPassword, "Passwords do not maatch")) {
                    return;
                }
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Updating");
                progressDialog.show();

                FirebaseAuth.getInstance().signInWithEmailAndPassword(FirebaseAuth.getInstance().getCurrentUser().getEmail(), textInputEditTextOldPassword.getText().toString())
                        .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                               if (!task.isSuccessful()) {
                                    progressDialog.dismiss();
                                    if (textInputEditTextOldPassword.getText().toString().length() < 6) {
                                        Toast.makeText(getActivity(), "Wrong Password", Toast.LENGTH_LONG).show();
                                    } else {
                                        Toast.makeText(getActivity(), "Authorization Failed", Toast.LENGTH_LONG).show();
                                    }
                                } else {

                                    progressDialog.dismiss();
                                   FirebaseAuth.getInstance().getCurrentUser().updatePassword(textInputEditTextPassword.getText().toString())
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {

                                    progressDialog.dismiss();
                                    Toast.makeText(getActivity(), "Successfully updated password!", Toast.LENGTH_LONG).show();
                                    Fragment fragment = new ManageProfile();
                                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                                    fragmentTransaction.replace(R.id.content_frame, fragment);
                                    fragmentTransaction.commit();
                                }
                            }
                        });

                        }      }
                        });
            }
        });
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Account Settings");

    }


}
