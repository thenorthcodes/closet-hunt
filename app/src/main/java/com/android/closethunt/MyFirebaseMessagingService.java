package com.android.closethunt;


import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by dell on 25-Jul-17.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    Notifications notifications = new Notifications();
    DBHelper dbHelper = new DBHelper(this);

    private static final String TAG = "MyFirebaseMsgService";
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        notifications.setBody(remoteMessage.getNotification().getBody());
        notifications.setTitle(remoteMessage.getNotification().getTitle());
        dbHelper.addToMsg(notifications);
    }

}
