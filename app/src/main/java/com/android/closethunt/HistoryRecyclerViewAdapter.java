package com.android.closethunt;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

/**
 * Created by dell on 12-Jul-17.
 */

public class HistoryRecyclerViewAdapter extends RecyclerView.Adapter<HistoryRecyclerViewAdapter.ViewHolder> {

    Context context;
    List<History> getDataAdapter;
    RecyclerView.Adapter recyclerViewadapter;
    DatabaseReference database;


    public HistoryRecyclerViewAdapter(List<History> getDataAdapter, Context context) {

        super();
        this.getDataAdapter = getDataAdapter;
        this.context = context;

    }

    @Override
    public HistoryRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_history, parent, false);
        HistoryRecyclerViewAdapter.ViewHolder viewHolder = new HistoryRecyclerViewAdapter.ViewHolder(v);
        database = FirebaseDatabase.getInstance().getReference().child("Order_History").child(FirebaseAuth.getInstance().getCurrentUser().getUid().toString());
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.id.append(getDataAdapter.get(position).getId());
        holder.amount.append(getDataAdapter.get(position).getAmount());
        recyclerViewadapter = new ListRecyclerViewAdapter(getDataAdapter.get(position).getDataAdapterList, context);
        holder.recyclerView.setAdapter(recyclerViewadapter);
        holder.status.append(getDataAdapter.get(position).getStatus());
        holder.cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                database.child(getDataAdapter.get(position).getId()).child("status").setValue("Cancelled");
//                getDataAdapter.clear();
                notifyDataSetChanged();

            }
        });
        holder.ret.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                database.child(getDataAdapter.get(position).getId()).child("status").setValue("Return");
//                getDataAdapter.clear();
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return getDataAdapter.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        View view;
        public TextView id, amount, status;
        RecyclerView recyclerView;
        Button cancel, ret;

        public ViewHolder(View itemView) {

            super(itemView);
            view = itemView;
            id = (TextView) view.findViewById(R.id.identity);
            amount = (TextView) view.findViewById(R.id.amount);
            status = (TextView) view.findViewById(R.id.status);
            recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView1);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            cancel = (Button) view.findViewById(R.id.cancel);
            ret = (Button) view.findViewById(R.id.ret);
        }


    }

}
