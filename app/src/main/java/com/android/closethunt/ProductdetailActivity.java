package com.android.closethunt;

import android.content.Intent;
import android.graphics.Paint;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import com.bumptech.glide.Glide;

import com.firebase.ui.storage.images.FirebaseImageLoader;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.List;

import static android.text.TextUtils.isEmpty;


public class ProductdetailActivity extends AppCompatActivity {

    public TextView Name, qty;
    public TextView Price;
    public TextView Color;
    public TextView Description;
    public TextView sizechart;
    public ToggleButton xs, s, m, l, xl;
    public TextView wrinkle;
    public ImageView img;
    DialogFragment sizech;
    String sizes;
    StorageReference storageReference;
    FirebaseStorage firebaseStorage;
    AppCompatButton add, del, viewcart;
    GetDataAdapter getDataAdapter;
    DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        firebaseStorage = FirebaseStorage.getInstance();
        Intent i = getIntent();
        sizes = i.getStringExtra("Sizes");
        String[] availsizes = sizes.split(" ");
        Name = (TextView) findViewById(R.id.textView0);
        Price = (TextView) findViewById(R.id.textView1);
        Color = (TextView) findViewById(R.id.textView4);
        Description = (TextView) findViewById(R.id.textView2);
        qty = (TextView) findViewById(R.id.qty);
//        Size = (TextView)findViewById(R.id.textView3a);
        xs = (ToggleButton) findViewById(R.id.textView3a);
        s =(ToggleButton)findViewById(R.id.textView3b) ;
        m = (ToggleButton)findViewById(R.id.textView3c);
        l = (ToggleButton)findViewById(R.id.textView3d);
        xl = (ToggleButton)findViewById(R.id.textView3e) ;
        wrinkle = (TextView)findViewById(R.id.textView6);
        sizechart =(TextView) findViewById(R.id.sizechart);
        sizechart.setPaintFlags(sizechart.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);

        sizechart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.d("AAAAAAAAAAAAA","clicked");
//                startActivity(new Intent(ProductdetailActivity.this, SizechartActivity.class));
                sizech = new SizechartActivity();
//                sizech.getDialog().getWindow().setLayout(50,50);
                sizech.show(getSupportFragmentManager(), "SIZES");
            }
        });

        img = (ImageView) findViewById(R.id.img);
        storageReference = firebaseStorage.getReference().child(i.getStringExtra("FileName"));

        Glide.with(getApplicationContext())
                .using(new FirebaseImageLoader())
                .load(storageReference)
                .placeholder(android.R.drawable.progress_indeterminate_horizontal)
                .into(img);

        Name.setText(i.getStringExtra("Name"));
        Price.setText(i.getStringExtra("Price"));
        Description.setText(i.getStringExtra("Description"));
        for(int j=0; j<availsizes.length; j++){
            if (availsizes[j].equalsIgnoreCase("XS")) xs.setEnabled(true);
            if (availsizes[j].equalsIgnoreCase("S")) s.setEnabled(true);
            if (availsizes[j].equalsIgnoreCase("M")) m.setEnabled(true);
            if (availsizes[j].equalsIgnoreCase("L")) l.setEnabled(true);
            if (availsizes[j].equalsIgnoreCase("Xl")) xl.setEnabled(true);

        }
        Color.setText(i.getStringExtra("Colors"));
        wrinkle.setText(i.getStringExtra("Features"));

        xs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(s.isChecked()) s.setChecked(false);
                if(m.isChecked()) m.setChecked(false);
                if(l.isChecked()) l.setChecked(false);
                if(xl.isChecked()) xl.setChecked(false);
                xs.setChecked(true);
            }
        });
        s.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(xs.isChecked()) xs.setChecked(false);
                if(m.isChecked()) m.setChecked(false);
                if(l.isChecked()) l.setChecked(false);
                if(xl.isChecked()) xl.setChecked(false);
                s.setChecked(true);

            }
        }); m.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(xs.isChecked()) xs.setChecked(false);
                if(s.isChecked()) s.setChecked(false);
                if(l.isChecked()) l.setChecked(false);
                if(xl.isChecked()) xl.setChecked(false);
                m.setChecked(true);

            }
        }); l.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(xs.isChecked()) xs.setChecked(false);
                if(s.isChecked()) s.setChecked(false);
                if(m.isChecked()) m.setChecked(false);
                if(xl.isChecked()) xl.setChecked(false);
                l.setChecked(true);

            }
        }); xl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(xs.isChecked()) xs.setChecked(false);
                if(s.isChecked()) s.setChecked(false);
                if(m.isChecked()) m.setChecked(false);
                if(l.isChecked()) l.setChecked(false);
                xl.setChecked(true);

            }
        });

//        getSupportActionBar().setHomeAsUpIndicator(R.drawable.icon_back_arrow);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        add = (AppCompatButton) findViewById(R.id.addcart);
        del = (AppCompatButton) findViewById(R.id.delcart);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String num = qty.getText().toString();
                int no = Integer.parseInt(num);
                no = no +1;
                qty.setText(String.valueOf(no));
            }
        });

        del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Integer.parseInt(qty.getText().toString())>1){
                    String num = qty.getText().toString();
                    int no = Integer.parseInt(num);
                    no = no - 1;
                    qty.setText(String.valueOf(no));
                }
                else Toast.makeText(getApplicationContext(), "Select 1 or More Items", Toast.LENGTH_LONG).show();

            }
        });


        viewcart = (AppCompatButton) findViewById(R.id.viewcart);
        viewcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String size ="";
                if(xs.isChecked()) size = "XS";
                if(s.isChecked()) size = "S";
                if(m.isChecked()) size = "M";
                if(l.isChecked()) size = "L";
                if(xl.isChecked()) size = "XL";

                if(isEmpty(size)||size.equalsIgnoreCase("")){
                    Toast.makeText(getApplicationContext(), "Please Select A Size!", Toast.LENGTH_LONG).show();
                }
                else{
                    getDataAdapter = new GetDataAdapter();
                    dbHelper = new DBHelper(getApplication());
                    getDataAdapter.setId(getIntent().getStringExtra("Id"));
                    getDataAdapter.setName(getIntent().getStringExtra("Name"));
                    getDataAdapter.setSize(size);
                    getDataAdapter.setFileName(getIntent().getStringExtra("FileName"));
                    getDataAdapter.setPrice(getIntent().getStringExtra("Price"));
                    getDataAdapter.setQty(Integer.parseInt(qty.getText().toString()));
                    List<GetDataAdapter> getDataAdapterList = dbHelper.getRow(getIntent().getStringExtra("Id"));
                    if(getDataAdapterList.isEmpty()) {dbHelper.addToCart(getDataAdapter);
                        Toast.makeText(getApplicationContext(), "Item(s) added to cart!", Toast.LENGTH_LONG).show();}
                    else{
                        int flag=0;
                        for(int k=0; k<getDataAdapterList.size(); k++){
                            if (getDataAdapterList.get(k).getSize().equalsIgnoreCase(size)) {
                                dbHelper.updateData(getDataAdapter);
                                Toast.makeText(getApplicationContext(), "Item(s) updated in cart!", Toast.LENGTH_LONG).show();
                                flag=1;
                                break;}
                        }
                        if(flag==0) {
                            dbHelper.addToCart(getDataAdapter);
                            Toast.makeText(getApplicationContext(), "Item(s) added to cart!", Toast.LENGTH_LONG).show();}
                    }

                    startActivity(new Intent(ProductdetailActivity.this, ViewcartActivity.class));
                }


            }
        });

    }
}
