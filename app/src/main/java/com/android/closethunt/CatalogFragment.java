package com.android.closethunt;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import it.moondroid.coverflow.components.ui.containers.FeatureCoverFlow;

/**
 * Created by dell on 18-Jul-17.
 */

public class CatalogFragment extends Fragment  {

    List<GetDataAdapter> GetDataAdapter1;
    RecyclerView recyclerView;
    RecyclerView.Adapter recyclerViewadapter;
    JSONObject jsonObject;
    JSONArray jsonArray;
    FloatingActionButton wishlist;
    private FeatureCoverFlow coverFlow;
    private CoverFlowAdapter adapter;
    private ArrayList<GetDataAdapter> games;
    int len=0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_main2, container, false);
        coverFlow = (FeatureCoverFlow) view.findViewById(R.id.coverflow);
        settingDummyData();
        adapter = new CoverFlowAdapter(getContext(), games);
        coverFlow.setAdapter(adapter);
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                coverFlow.scroll(1);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 2500, 2500);
        coverFlow.setOnScrollPositionListener(onScrollListener());

        GetDataAdapter1 = new ArrayList<>();
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView1);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        jsonObject = getJSonData();
        try {
            jsonArray = jsonObject.getJSONArray("Products");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        getArrayData(jsonArray);


        wishlist = (FloatingActionButton) view.findViewById(R.id.viewwishlist);
        wishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), WishlistActivity.class));
            }
        });
        return view;
    }
    private JSONObject getJSonData(){

        JSONObject jsonObject=null;

        try {

            InputStream is = getResources().getAssets().open("Clothing.json");
            int size = is.available();
            byte[] data = new byte[size];
            is.read(data);
            is.close();
            String json = new String(data, "UTF-8");
            jsonObject=new JSONObject(json);

        }catch (IOException e){

            e.printStackTrace();

        }catch (JSONException je){

            je.printStackTrace();

        }

        return jsonObject;

    }

    public void getArrayData(JSONArray array){

        for(int i = 0; i<array.length(); i++) {
            final GetDataAdapter GetDataAdapter2 = new GetDataAdapter();
            JSONObject json = null, json1 = null, json2 = null;
            JSONArray size, features;
            String sizes="";
            try {
                json = array.getJSONObject(i);
                size = json.getJSONArray("SizeAvail");
                features = json.getJSONArray("Features");
                GetDataAdapter2.setName(json.getString("Name"));
                GetDataAdapter2.setId(json.getString("id"));
                GetDataAdapter2.setColor(json.getJSONObject("ColorAvail").getString("Color"));
                GetDataAdapter2.setPrice(json.getString("Price"));
                GetDataAdapter2.setDiscount(json.getString("Discount"));
                GetDataAdapter2.setDetail(json.getString("Details"));
                GetDataAdapter2.setDescription(json.getString("Description"));
                GetDataAdapter2.setFileName(json.getString("Image"));

                for(int j=0; j<size.length(); j++){
                    json1 = size.getJSONObject(j);
                    sizes = sizes + json1.getString("Size")+"   ";
                }
                GetDataAdapter2.setSize(sizes);
                if(features.getJSONObject(0).getString("Wrinkle_free").equalsIgnoreCase("true")){GetDataAdapter2.setWrinkle_free(true);}
                if(features.getJSONObject(1).getString("Stretch").equalsIgnoreCase("true")){GetDataAdapter2.setStretch(true);}
                if(features.getJSONObject(3).getString("Lined").equalsIgnoreCase("true")){GetDataAdapter2.setLined(true);}
                if(features.getJSONObject(4).getString("Sheer").equalsIgnoreCase("true")){GetDataAdapter2.setSheer(true);}
                if(features.getJSONObject(5).getString("Machine_Wash").equalsIgnoreCase("true")){GetDataAdapter2.setMachine_wash(true);}


            } catch (JSONException e) {

                e.printStackTrace();
            }
            GetDataAdapter1.add(GetDataAdapter2);
        }

        recyclerViewadapter = new RecyclerViewAdapter(GetDataAdapter1, getContext());
        recyclerView.setAdapter(recyclerViewadapter);

    }



    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Shop Catalog");
    }

    private FeatureCoverFlow.OnScrollPositionListener onScrollListener() {
        return new FeatureCoverFlow.OnScrollPositionListener() {
            @Override
            public void onScrolledToPosition(int position) {
                Log.v("MainActiivty", "position: " + position);
            }

            @Override
            public void onScrolling() {
                Log.i("MainActivity", "scrolling");
            }
        };
    }


    private void settingDummyData() {
        games = new ArrayList<>();

        games.add(new GetDataAdapter(R.drawable.caru2));
        games.add(new GetDataAdapter(R.drawable.caru0));
        games.add(new GetDataAdapter(R.drawable.caru1));
        games.add(new GetDataAdapter(R.drawable.caru3));
        games.add(new GetDataAdapter(R.drawable.caru4));
        games.add(new GetDataAdapter(R.drawable.caru5));

        len = games.size();



    }

}