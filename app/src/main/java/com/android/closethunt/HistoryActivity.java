package com.android.closethunt;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class HistoryActivity extends AppCompatActivity {

    List<History> historyList;
    RecyclerView recyclerView;
    RecyclerView.Adapter recyclerViewadapter;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        historyList = new ArrayList<>();
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView1);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        databaseReference = FirebaseDatabase.getInstance().getReference().child("Order_History").child(FirebaseAuth.getInstance().getCurrentUser().getUid().toString());
        databaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    History history = new History();
                    List<GetDataAdapter> gdalist = new ArrayList<GetDataAdapter>();
                    history.setAmount(String.valueOf(dataSnapshot.child("Amount").getValue()));
                    history.setId((String) dataSnapshot.child("Id").getValue());
                    for(DataSnapshot ds:dataSnapshot.child("Products").getChildren()){
                        String name = (String) ds.child("name").getValue();
                        String qty = (String) ds.child("qty").getValue();
                        String size = (String) ds.child("size").getValue() ;
                        String price = String.valueOf(ds.child("price").getValue());
                        gdalist.add(new GetDataAdapter(qty, name, price, size));
                    }
                    history.setGetDataAdapterList(gdalist);
                    history.setStatus((String) dataSnapshot.child("status").getValue());
                    historyList.add(history);

                recyclerViewadapter = new HistoryRecyclerViewAdapter(historyList, getApplicationContext());
                recyclerView.setAdapter(recyclerViewadapter);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(recyclerViewadapter!=null){
                    recyclerViewadapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }





}
