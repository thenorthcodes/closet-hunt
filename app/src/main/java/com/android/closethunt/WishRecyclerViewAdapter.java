package com.android.closethunt;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.List;

/**
 * Created by dell on 05-Jul-17.
 */

public class WishRecyclerViewAdapter extends RecyclerView.Adapter<WishRecyclerViewAdapter.ViewHolder> {
    Context context;
    List<GetDataAdapter> getDataAdapter;
    StorageReference storageReference;
    String fileName;

    public WishRecyclerViewAdapter(List<GetDataAdapter> getDataAdapter, Context context) {

        super();
        this.getDataAdapter = getDataAdapter;
        this.context = context;
    }

    @Override
    public WishRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_row, parent, false);
        WishRecyclerViewAdapter.ViewHolder viewHolder = new WishRecyclerViewAdapter.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final WishRecyclerViewAdapter.ViewHolder holder, final int position) {
        holder.Name.setText(getDataAdapter.get(position).getName());
        holder.Price.setText(getDataAdapter.get(position).getPrice());
        holder.Detail.setText(getDataAdapter.get(position).getDetail());
        holder.toggleButton.setVisibility(View.GONE);

        fileName = getDataAdapter.get(position).getFileName();
        storageReference = FirebaseStorage.getInstance().getReference().child(fileName);

        Glide.with(context)
                .using(new FirebaseImageLoader())
                .load(storageReference)
                .placeholder(android.R.drawable.progress_indeterminate_horizontal)
                .into(holder.img);


    }

    @Override
    public int getItemCount() {
        return getDataAdapter.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView Name;
        public TextView Price, Detail;
        public ImageView img;
        View view;
        ToggleButton toggleButton;

        public ViewHolder(View itemView) {

            super(itemView);
            view = itemView;
            Name = (TextView) itemView.findViewById(R.id.textView0);
            Price = (TextView) itemView.findViewById(R.id.textView1);
            Detail = (TextView) itemView.findViewById(R.id.textView100);
            img = (ImageView) itemView.findViewById(R.id.img);
            toggleButton = (ToggleButton) itemView.findViewById(R.id.toggleheart);



        }


    }

}
