package com.android.closethunt;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ProfileActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener{

    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth auth;
    private final AppCompatActivity activity = ProfileActivity.this;
    private Button ButtonLogout;
    ActionBarDrawerToggle toggle;
    DatabaseReference database;
    View view;
    DrawerLayout drawer;
    ImageView imageView;
    NavigationView navigationView;
    DBHelper dbHelper;
    int count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prof);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        dbHelper = new DBHelper(getApplicationContext());
        count = dbHelper.cart_count();
        database = FirebaseDatabase.getInstance().getReference().child("Users");
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        imageView = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.imageView);
        database.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.getKey().equalsIgnoreCase(FirebaseAuth.getInstance().getCurrentUser().getUid()))  {
                    User user = dataSnapshot.getValue(User.class);
                    if(user.getGender()!=null){
                        if(user.getGender().equalsIgnoreCase("female")){
                        imageView.setImageResource(R.drawable.ic_face_black_24dp);
                        }
                        else if(user.getGender().equalsIgnoreCase("male")){
                            imageView.setImageResource(R.drawable.ic_female);
                        }
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.getKey().equalsIgnoreCase(FirebaseAuth.getInstance().getCurrentUser().getUid()))  {
                    User user = dataSnapshot.getValue(User.class);
                    if(user.getGender()!=null){
                        if(user.getGender().equalsIgnoreCase("female")){
                            imageView.setImageResource(R.drawable.ic_face_black_24dp);
                        }
                        else if(user.getGender().equalsIgnoreCase("male")){
                            imageView.setImageResource(R.drawable.ic_female);
                        }
                    }
                }

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        auth = FirebaseAuth.getInstance();
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {

            startActivity(new Intent(ProfileActivity.this, MainActivity.class));
            finish();
        }
        //success_email.setText(user.getEmail());
        ButtonLogout = (Button) findViewById(R.id.logs);
        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    // user auth state is changed - user is null
                    // launch login activity
                    startActivity(new Intent(ProfileActivity.this, MainActivity.class));
                    finish();
                }
            }
        };

        ButtonLogout.setOnClickListener(this);



        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(true);

        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        if(getIntent().getStringExtra("Screen")==null){
            displaySelectedScreen(R.id.shop);
        }else if(getIntent().getStringExtra("Screen").equalsIgnoreCase("Profile") ){

            displaySelectedScreen(R.id.nav_manage_profile);

        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

//            super.onBackPressed();
            logout();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.prof, menu);
        if(count>0){
            menu.findItem(R.id.cartmenu).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        } else{
            menu.findItem(R.id.cartmenu).setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.update_password) {
            displaySelectedScreen(R.id.nav_change_picture);
//            return true;
        }

        if (id == android.R.id.home) {
            onBackPressed();
//            return true;
        }

        if(id==R.id.cartmenu){
            startActivity(new Intent(ProfileActivity.this, ViewcartActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        displaySelectedScreen(item.getItemId());
        return true;
    }

   @Override
    public void onStart() {
        super.onStart();
        auth.addAuthStateListener(authListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (authListener != null) {
            auth.removeAuthStateListener(authListener);
        }
    }

    private void displaySelectedScreen(int itemId) {

        //creating fragment object
        Fragment fragment = null;

        //initializing the fragment object which is selected
        switch (itemId) {

            case R.id.nav_manage_profile:
                fragment = new ManageProfile();
                break;


            case R.id.nav_profile:
                startActivity(new Intent(ProfileActivity.this, ViewprofileActivity.class));
                break;

            case R.id.nav_change_picture:
                fragment = new Updatepassword_Frag();
                break;

            case R.id.nav_json:
                break;

            case R.id.orderhistory:
                startActivity(new Intent(ProfileActivity.this, HistoryActivity.class));
                break;

            case R.id.shop:
                fragment = new CatalogFragment();
                break;

            case R.id.notif:
                startActivity(new Intent(ProfileActivity.this, NotificationActivity.class));
                break;

            case R.id.aboutus:
                startActivity(new Intent(ProfileActivity.this, AboutusActivity.class));
                break;

            case R.id.faq:
                startActivity(new Intent(ProfileActivity.this, FaqActivity.class));
                break;

        }

        //replacing the fragment
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

            ft.replace(R.id.content_frame, fragment).addToBackStack("iykhv").commit();
//            ft.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.logs: logout();
                break;

        }
    }

    public void logout(){
        // 1. Instantiate an AlertDialog.Builder with its constructor
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);

// 2. Chain together various setter methods to set the dialog characteristics
        builder.setMessage("Are you sure you want to sign out?")
                .setTitle("Logout");

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                auth.signOut();
                startActivity(new Intent(ProfileActivity.this, MainActivity.class));
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                displaySelectedScreen(R.id.shop);

            }
        });

// 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
