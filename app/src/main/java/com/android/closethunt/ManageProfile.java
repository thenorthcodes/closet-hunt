package com.android.closethunt;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.closethunt.R;
import com.firebase.client.Firebase;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static android.text.TextUtils.isEmpty;

/**
 * Created by dell on 09-Jun-17.
 */

public class ManageProfile extends Fragment {


    private EditText textInputEditTextName;
    private EditText textInputEditTextAddress1;
    private EditText textInputEditTextAddress2;
    private EditText textInputEditTextAddress3;
    private EditText textInputEditTextPhone;
    FirebaseUser userf;
    Button appCompatButtonSubmit;
    DatabaseReference database;
    String getid, eml, userId;
    private RadioGroup gender;
    private RadioButton selectedrb;
    View view;
    RadioButton male, female;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_manage_profile, container, false);

        textInputEditTextName = (EditText) view.findViewById(R.id.textInputEditTextName);
        textInputEditTextAddress1 = (EditText)view.findViewById(R.id.textInputEditTextAddress1);
        textInputEditTextAddress2 = (EditText) view.findViewById(R.id.textInputEditTextAddress2);
        textInputEditTextAddress3 = (EditText) view.findViewById(R.id.textInputEditTextAddress3);
        textInputEditTextPhone = (EditText) view.findViewById(R.id.textInputEditTextPhone);
        appCompatButtonSubmit = (Button) view.findViewById(R.id.appCompatButtonSubmit);
        gender = (RadioGroup) view.findViewById(R.id.RBG);
        male = (RadioButton) view.findViewById(R.id.male);
        female =(RadioButton) view.findViewById(R.id.female);



        userf = FirebaseAuth.getInstance().getCurrentUser();
        if(userf!=null){
            eml = userf.getEmail();
             userId = userf.getUid();
        }

        appCompatButtonSubmit.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    appCompatButtonSubmit.setBackgroundColor(Color.BLACK);
                }
                return false;
            }

        });
        Firebase.setAndroidContext(getContext());
        database = FirebaseDatabase.getInstance().getReference().child("Users");
        database.addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.getKey().equalsIgnoreCase(userId)){
                    User user = dataSnapshot.getValue(User.class);
                    textInputEditTextName.setText(user.getName());
                    textInputEditTextAddress1.setText(user.getAddress1());
                    textInputEditTextAddress2.setText(user.getAddress2());
                    textInputEditTextAddress3.setText(user.getAddress3());
                    textInputEditTextPhone.setText(user.getPhone());
                    if(user.getGender().equalsIgnoreCase("female")){
                        female.setChecked(true);                    }
                    else if(user.getGender().equalsIgnoreCase("male")){
                        male.setChecked(true);
                    }

//
                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });





        appCompatButtonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = textInputEditTextName.getText().toString().trim();
                String address1 = textInputEditTextAddress1.getText().toString().trim();
                String address2 = textInputEditTextAddress2.getText().toString().trim();
                String address3 = textInputEditTextAddress3.getText().toString().trim();
                String phone = textInputEditTextPhone.getText().toString().trim();
                int selectedId = gender.getCheckedRadioButtonId();
                selectedrb = (RadioButton) view.findViewById(selectedId);
                String gen = selectedrb.getText().toString().trim();
                getid = database.push().getKey();


                //Adding values
                if(!isEmpty(name)){
                    database.child(userId).child("name").setValue(name);
                }
                if(!isEmpty(address1)){
                    database.child(userId).child("address1").setValue(address1);

                }
                if(!isEmpty(address2)){
                    database.child(userId).child("address2").setValue(address2);

                }
                if(!isEmpty(address3)){
                    database.child(userId).child("address3").setValue(address3);

                }
                if(!isEmpty(phone)){
                    database.child(userId).child("phone").setValue(phone);

                }
                if(!isEmpty(gen)){
                    database.child(userId).child("gender").setValue(gen);

                }

                database.child(userId).child("email").setValue(eml);

                //Storing values to firebase
                //ref.child("User").child(userId).setValue(user);
                //database.child(getid).setValue(user);
               // database.child(userId).setValue(user);
                Toast.makeText(getActivity(), "Successfully updated details!", Toast.LENGTH_LONG).show();
                /*Fragment fragment = new ManageProfile();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, fragment);
                fragmentTransaction.commit();*/
                startActivity(new Intent(getContext(), ViewprofileActivity.class));
            }
        });
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Edit Profile");
    }



    }



