package com.android.closethunt;

import java.util.List;

/**
 * Created by dell on 12-Jul-17.
 */

public class History {
    public String id, amount;
    public List<GetDataAdapter> getDataAdapterList;
    String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public List<GetDataAdapter> getGetDataAdapterList() {
        return getDataAdapterList;
    }

    public void setGetDataAdapterList(List<GetDataAdapter> getDataAdapterList) {
        this.getDataAdapterList = getDataAdapterList;
    }
}
