package com.android.closethunt;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by dell on 25-Jul-17.
 */

public class NotifRecyclerViewAdapter extends RecyclerView.Adapter<NotifRecyclerViewAdapter.ViewHolder> {

    Context context;
    List<Notifications> notificationsList;

    public NotifRecyclerViewAdapter(List<Notifications> notificationses, Context context) {

        super();
        this.notificationsList = notificationses;
        this.context = context;

    }

    @Override
    public NotifRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_notif, parent, false);
        NotifRecyclerViewAdapter.ViewHolder viewHolder = new NotifRecyclerViewAdapter.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.body.setText(notificationsList.get(position).getBody());
        holder.title.setText(notificationsList.get(position).getTitle());

    }


    @Override
    public int getItemCount() {
        return notificationsList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView body, title;

        public ViewHolder(View itemView) {

            super(itemView);
           body = (TextView) itemView.findViewById(R.id.body);
           title = (TextView) itemView.findViewById(R.id.title);

        }


    }


}
