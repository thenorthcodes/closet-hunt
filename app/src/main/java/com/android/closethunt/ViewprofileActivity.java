package com.android.closethunt;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.firebase.client.Firebase;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ViewprofileActivity extends AppCompatActivity {

    private TextView textInputEditTextName;
    private TextView textInputEditTextAddress1;
    private TextView textInputEditTextAddress2;
    private TextView textInputEditTextAddress3;
    private TextView textInputEditTextPhone;
    private TextView textInputEditTextGender;

    FirebaseUser userf;
    Button appCompatButtonSubmit;
    DatabaseReference database;
    String getid, eml, userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewprofile);
		 getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        textInputEditTextName = (TextView) findViewById(R.id.textInputEditTextName);
        textInputEditTextAddress1 = (TextView) findViewById(R.id.textInputEditTextAddress1);
        textInputEditTextAddress2 = (TextView) findViewById(R.id.textInputEditTextAddress2);
        textInputEditTextAddress3 = (TextView) findViewById(R.id.textInputEditTextAddress3);
        textInputEditTextPhone = (TextView) findViewById(R.id.textInputEditTextPhone);
        textInputEditTextGender = (TextView) findViewById(R.id.textInputEditTextGender);
        appCompatButtonSubmit = (Button) findViewById(R.id.appCompatButtonEdit);

        userf = FirebaseAuth.getInstance().getCurrentUser();
        if(userf!=null){
            eml = userf.getEmail();
            userId = userf.getUid();
        }

        Firebase.setAndroidContext(this);
        database = FirebaseDatabase.getInstance().getReference().child("Users");
        database.addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.getKey().equalsIgnoreCase(userId)){
                    User user = dataSnapshot.getValue(User.class);
                    textInputEditTextName.setText(user.getName());
                    textInputEditTextAddress1.setText(user.getAddress1());
                    textInputEditTextAddress2.setText(user.getAddress2());
                    textInputEditTextAddress3.setText(user.getAddress3());
                    textInputEditTextPhone.setText(user.getPhone());
                    textInputEditTextGender.setText(user.getGender());
//
                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        appCompatButtonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewprofileActivity.this, ProfileActivity.class);
                intent.putExtra("Screen", "Profile");
                startActivityForResult(intent, RESULT_OK);
            }
        });
    }



}

