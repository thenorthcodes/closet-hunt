package com.android.closethunt;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by dell on 12-Jul-17.
 */

public class ListRecyclerViewAdapter extends RecyclerView.Adapter<ListRecyclerViewAdapter.ViewHolder> {

    Context context;
    List<GetDataAdapter> getDataAdapter;

    public ListRecyclerViewAdapter(List<GetDataAdapter> getDataAdapter, Context context) {

        super();
        this.getDataAdapter = getDataAdapter;
        this.context = context;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.prod_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ListRecyclerViewAdapter.ViewHolder holder, int position) {
        holder.name.append(getDataAdapter.get(position).getName());
        holder.qty.append(getDataAdapter.get(position).getQuantity());
        holder.size.append(getDataAdapter.get(position).getSize());
        holder.price.append(getDataAdapter.get(position).getPrice());
    }


    @Override
    public int getItemCount() {
        return getDataAdapter.size();
    }



    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name, size, qty, price;

        public ViewHolder(View itemView) {

            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            size = (TextView) itemView.findViewById(R.id.size);
            price = (TextView) itemView.findViewById(R.id.price);
            qty = (TextView) itemView.findViewById(R.id.qty);
        }


    }

}
