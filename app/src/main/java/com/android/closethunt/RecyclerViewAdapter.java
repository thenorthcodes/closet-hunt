package com.android.closethunt;

import android.content.Context;
import android.content.Intent;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.List;

/**
 * Created by dell on 21-Jun-17.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    Context context;
    List<GetDataAdapter> getDataAdapter;
    StorageReference storageReference;
    String fileName;
    DBHelper dbHelper ;



    public RecyclerViewAdapter(List<GetDataAdapter> getDataAdapter, Context context) {

        super();
        this.getDataAdapter = getDataAdapter;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        dbHelper = new DBHelper(context);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        String feat = "";
        holder.Name.setText(getDataAdapter.get(position).getName());
        holder.Price.setText(getDataAdapter.get(position).getPrice());
        holder.Detail.setText(getDataAdapter.get(position).getDetail());

        if(getDataAdapter.get(position).getStretch()) feat = feat +"Stretchable ";
        if(getDataAdapter.get(position).getSheer()) feat = feat +"Sheer ";
        if(getDataAdapter.get(position).getWrinkle_free()) feat = feat +"Wrinklefree ";
        if(getDataAdapter.get(position).getMachine_wash())  feat = feat +"MachineWashable ";
        if(getDataAdapter.get(position).getLined())feat = feat +"Lined ";
        getDataAdapter.get(position).setFeatures(feat);
        fileName = getDataAdapter.get(position).getFileName();
        storageReference = FirebaseStorage.getInstance().getReference().child(fileName);

        Glide.with(context)
                .using(new FirebaseImageLoader())
                .load(storageReference)
                .placeholder(android.R.drawable.progress_indeterminate_horizontal)
                .into(holder.img);


       /* getDataAdapterList = dbHelper.getAllWishlistItems();

        for(int k=0; k<getDataAdapterList.size();k++){
            if(getDataAdapter.get(position).getId().equalsIgnoreCase(getDataAdapterList.get(k).getId())){
                holder.toggleButton.setChecked(true);
                holder.toggleButton.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_black_24dp));
            } else{
                holder.toggleButton.setChecked(false);
                holder.toggleButton.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_black_24dp));
            }

        }*/

        dbHelper = new DBHelper(context);
        if(dbHelper.isInWishlist(getDataAdapter.get(position).getId())){
            holder.toggleButton.setChecked(true);
            holder.toggleButton.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.favorite_heart_button));

        } else{
            holder.toggleButton.setChecked(false);
            holder.toggleButton.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.heart));

        }


    }

    @Override
    public int getItemCount() {
        return getDataAdapter.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView Name;
        public TextView Price, Detail;
        public ImageView img;
        public ToggleButton toggleButton;
        View view;

        public ViewHolder(View itemView) {

            super(itemView);
            view = itemView;
            Name = (TextView) itemView.findViewById(R.id.textView0);
            Price = (TextView) itemView.findViewById(R.id.textView1);
            Detail = (TextView) itemView.findViewById(R.id.textView100);
            img = (ImageView) itemView.findViewById(R.id.img);
            toggleButton = (ToggleButton) itemView.findViewById(R.id.toggleheart);

           /* toggleButton.setChecked(false);*/
            toggleButton.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.favorite_heart_button));


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, ProductdetailActivity.class);
                    i.putExtra("Name",getDataAdapter.get(getAdapterPosition()).getName());
                    i.putExtra("Price",getDataAdapter.get(getAdapterPosition()).getPrice());
                    i.putExtra("Sizes",getDataAdapter.get(getAdapterPosition()).getSize());
                    i.putExtra("Colors",getDataAdapter.get(getAdapterPosition()).getColor());
                    i.putExtra("Features", getDataAdapter.get(getAdapterPosition()).getFeatures());
                    i.putExtra("Description",getDataAdapter.get(getAdapterPosition()).getDescription());
                    i.putExtra("FileName",getDataAdapter.get(getAdapterPosition()).getFileName());
                    i.putExtra("Id",getDataAdapter.get(getAdapterPosition()).getId());
                    context.startActivity(i);
                }
            });

            toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        toggleButton.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.favorite_heart_button));
                        dbHelper = new DBHelper(context);
                        dbHelper.addToWishlist(getDataAdapter.get(getAdapterPosition()));
                    }
                    else {
                        dbHelper = new DBHelper(context);
                        toggleButton.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.heart));
                            dbHelper.removeFromWishlist(getDataAdapter.get(getAdapterPosition()).getId());


                    }
                }
            });



        }


    }

}
