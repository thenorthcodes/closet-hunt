package com.android.closethunt;


/**
 * Created by dell on 21-Jun-17.
 */

public class GetDataAdapter {

    int qty;

    private int imageSource;

    public int getImageSource() {
        return imageSource;
    }


    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    String status = "Dispatched";

    String id;
    String name;
    String color;
    String price;
    String discount;
    String description;
    String Quantity;
    String size;
    String detail;
    String features;
    String fileName;
    Boolean wrinkle_free=false,  machine_wash=false, stretch=false, lined=false, sheer=false;


    public GetDataAdapter (int imageSource) {
        this.imageSource = imageSource;
    }

    public GetDataAdapter() {
    }

    public String getFeatures() {
        return features;
    }

    public void setFeatures(String features) {
        this.features = features;
    }


    public String getDetail() {
        return detail;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }



    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }


    public void setDiscount(String discount) {
        this.discount = discount;
    }



    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public GetDataAdapter(String qty, String name,  String price, String size) {
        Quantity = qty;
        this.name = name;
        this.price = price;
        this.size = size;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }



    public Boolean getWrinkle_free() {
        return wrinkle_free;
    }

    public void setWrinkle_free(Boolean wrinkle_free) {
        this.wrinkle_free = wrinkle_free;
    }

    public Boolean getMachine_wash() {
        return machine_wash;
    }

    public void setMachine_wash(Boolean machine_wash) {
        this.machine_wash = machine_wash;
    }

    public Boolean getStretch() {
        return stretch;
    }

    public void setStretch(Boolean stretch) {
        this.stretch = stretch;
    }

    public Boolean getLined() {
        return lined;
    }

    public void setLined(Boolean lined) {
        this.lined = lined;
    }

    public Boolean getSheer() {
        return sheer;
    }

    public void setSheer(Boolean sheer) {
        this.sheer = sheer;
    }

    public String getQuantity() {return Quantity;}
}
