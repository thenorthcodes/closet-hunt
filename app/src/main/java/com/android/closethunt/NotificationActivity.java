package com.android.closethunt;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class NotificationActivity extends AppCompatActivity {

    DBHelper dbHelper;
    List<Notifications> notificationsList;
    RecyclerView recyclerView;
    RecyclerView.Adapter notifyAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        dbHelper = new DBHelper(this);
        notificationsList = new ArrayList<>();
        notificationsList= dbHelper.getAllNotifs();
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView1);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        if(notificationsList.size()>0){
            notifyAdapter = new NotifRecyclerViewAdapter(notificationsList, this);
            recyclerView.setAdapter(notifyAdapter);
        } else{
            Toast.makeText(this, "No Notifications", Toast.LENGTH_SHORT).show();
            onBackPressed();
        }

        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT){

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                DBHelper dbHelper = new DBHelper(getApplicationContext());
                dbHelper.removeFromNotifs(notificationsList.get(viewHolder.getAdapterPosition()).getId());
                notificationsList.remove(notificationsList.get(viewHolder.getAdapterPosition()));
                notifyAdapter.notifyDataSetChanged();
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);

        if (notifyAdapter!= null)
            notifyAdapter.notifyDataSetChanged();
    }
}
