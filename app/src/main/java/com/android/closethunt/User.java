package com.android.closethunt;

/**
 * Created by dell on 10-Jun-17.
 */

public class User {

    private String name;
    private String address1;
    private String address2;
    private String address3;
    private String phone;
    private String email;
    private String gender;


    public User() {
      /*Blank default constructor essential for Firebase*/
    }
    //Getters and setters
    public String getName() {
        return name;
    }

    public String getEmail() { return email;}

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) { this.email = email;}

    public String getAddress1() {
        return address1;
    }

    public String getAddress2() {
        return address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setGender(String gender){this.gender = gender;}

    public String getGender(){ return  gender;}

}
