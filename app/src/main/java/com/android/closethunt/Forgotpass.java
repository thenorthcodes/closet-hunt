package com.android.closethunt;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.android.closethunt.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import static android.text.TextUtils.isEmpty;

public class Forgotpass extends AppCompatActivity {

    private EditText inputEmail;
    private Button btnReset;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotpass);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        inputEmail = (EditText) findViewById(R.id.email);
        btnReset = (Button) findViewById(R.id.btn_reset_password);
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = inputEmail.getText().toString().trim();

                if (isEmpty(email)) {
                    Toast.makeText(Forgotpass.this, "Enter your registered email id", Toast.LENGTH_SHORT).show();
                    return;
                }

                progressDialog = new ProgressDialog(Forgotpass.this);
                progressDialog.setMessage("Verifiying");
                progressDialog.show();
                FirebaseAuth.getInstance().sendPasswordResetEmail(email)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(Forgotpass.this, "We have sent you instructions to reset your password!", Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(Forgotpass.this, "Failed to send reset email!", Toast.LENGTH_LONG).show();
                                }
                                progressDialog.dismiss();
                                inputEmail.setText(null);
                                startActivity(new Intent(Forgotpass.this, MainActivity.class));
                                finish();
                            }
                        });
            }     });
    }
}
