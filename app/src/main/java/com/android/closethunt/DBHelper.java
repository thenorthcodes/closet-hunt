package com.android.closethunt;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dell on 02-Jul-17.
 */

public class DBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 5;
    // Database Name
    private static final String DATABASE_NAME = "closethunt";
    // order table name
    private static final String TABLE_ORDERS = "orders";
	private static final String TABLE_WISHLIST = "wishlist";
    private static final String TABLE_NOTIFS = "notifs";
    // order Table Columns names
    private static final String PROD_ID = "id";
    private static final String PROD_NAME = "name";
    private static final String PROD_PRICE = "price";
    private static final String PROD_QTY= "quantity";
    private static final String PROD_SIZE = "size";
    private static final String PROD_FILE = "filename";
    private static final String PROD_DETAIL = "details";
    private static final String MSG_BODY = "body";
    private static final String MSG_TITLE = "title";



    String CREATE_ORDERS_TABLE = "CREATE TABLE " + TABLE_ORDERS + " (" + "uni_id INTEGER PRIMARY KEY AUTOINCREMENT,"
            + PROD_ID + " STRING ," + PROD_NAME +" TEXT,"
            + PROD_PRICE + " TEXT," + PROD_QTY + " INTEGER," + PROD_FILE+ " STRING," + PROD_SIZE + " TEXT"+ ")";
			
	String CREATE_WISHLIST_TABLE = "CREATE TABLE " + TABLE_WISHLIST + " ("
            + PROD_ID + " STRING PRIMARY KEY," + PROD_NAME +" TEXT,"
            + PROD_PRICE + " TEXT,"+ PROD_FILE + " STRING," + PROD_DETAIL + " TEXT"+ ")";

    String CREATE_NOTIFS_TABLE = "CREATE TABLE " + TABLE_NOTIFS + " ("+ "id INTEGER PRIMARY KEY AUTOINCREMENT,"
            + MSG_BODY + " STRING," +  MSG_TITLE +" STRING" + ")";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_ORDERS_TABLE);
		db.execSQL(CREATE_WISHLIST_TABLE);
        db.execSQL(CREATE_NOTIFS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORDERS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_WISHLIST);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTIFS);

// Creating tables again
        onCreate(db);
    }

    public void addToMsg(Notifications notifications){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(MSG_BODY, notifications.getBody());
        values.put(MSG_TITLE, notifications.getTitle());
        db.insert(TABLE_NOTIFS, null, values);
        db.close();
    }

    public void addToCart(GetDataAdapter getDataAdapter){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(PROD_ID, getDataAdapter.getId());
        values.put(PROD_NAME, getDataAdapter.getName());
        values.put(PROD_PRICE, getDataAdapter.getPrice());
        values.put(PROD_QTY, getDataAdapter.getQty());
        values.put(PROD_SIZE, getDataAdapter.getSize());
        values.put(PROD_FILE, getDataAdapter.getFileName());
        // Inserting Row
        db.insert(TABLE_ORDERS, null, values);
        db.close();
    }
	
	public void addToWishlist(GetDataAdapter getDataAdapter){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(PROD_ID, getDataAdapter.getId());
        values.put(PROD_NAME, getDataAdapter.getName());
        values.put(PROD_PRICE, getDataAdapter.getPrice());
        values.put(PROD_FILE, getDataAdapter.getFileName());
        values.put(PROD_DETAIL, getDataAdapter.getDetail());

        // Inserting Row
        db.insert(TABLE_WISHLIST, null, values);
        db.close();
    }

    public void removeFromCart(String id, String size){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_ORDERS, PROD_ID+"=? AND "+PROD_SIZE+"=?", new String[]{id, size});
        db.close();
    }
	
	public void removeFromWishlist(String id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_WISHLIST, PROD_ID+"=?", new String[]{id});
        db.close();
    }

    public void removeFromNotifs(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NOTIFS, "id=?", new String[]{String.valueOf(id)});
        db.close();
    }

    public void emptyCart(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ TABLE_ORDERS);
        db.close();
    }

    public List<GetDataAdapter> getAllCartItems(){
        String[] columns = {
                PROD_ID,
                PROD_PRICE,
                PROD_NAME,
                PROD_QTY,
                PROD_SIZE,
                PROD_FILE
        };

        String sortOrder = PROD_NAME + " ASC";
        List<GetDataAdapter> getDataAdapterList = new ArrayList<GetDataAdapter>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_ORDERS, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                sortOrder); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                GetDataAdapter getDataAdapter = new GetDataAdapter();
                getDataAdapter.setId(cursor.getString(cursor.getColumnIndex(PROD_ID)));
                getDataAdapter.setName(cursor.getString(cursor.getColumnIndex(PROD_NAME)));
                getDataAdapter.setPrice(cursor.getString(cursor.getColumnIndex(PROD_PRICE)));
                getDataAdapter.setQty(Integer.parseInt(cursor.getString(cursor.getColumnIndex(PROD_QTY))));
                getDataAdapter.setSize(cursor.getString(cursor.getColumnIndex(PROD_SIZE)));
                getDataAdapter.setFileName(cursor.getString(cursor.getColumnIndex(PROD_FILE)));
                // Adding user record to list
                getDataAdapterList.add(getDataAdapter);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return getDataAdapterList;

    }
	
	
	public List<GetDataAdapter> getAllWishlistItems(){
        String[] columns = {
                PROD_ID,
                PROD_PRICE,
                PROD_NAME,
                PROD_DETAIL,
                PROD_FILE
        };

        String sortOrder = PROD_NAME + " ASC";
        List<GetDataAdapter> getDataAdapterList = new ArrayList<GetDataAdapter>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_WISHLIST, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                sortOrder); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                GetDataAdapter getDataAdapter = new GetDataAdapter();
                getDataAdapter.setId(cursor.getString(cursor.getColumnIndex(PROD_ID)));
                getDataAdapter.setName(cursor.getString(cursor.getColumnIndex(PROD_NAME)));
                getDataAdapter.setPrice(cursor.getString(cursor.getColumnIndex(PROD_PRICE)));
                getDataAdapter.setFileName(cursor.getString(cursor.getColumnIndex(PROD_FILE)));
                getDataAdapter.setDetail(cursor.getString(cursor.getColumnIndex(PROD_DETAIL)));
                
                // Adding user record to list
                getDataAdapterList.add(getDataAdapter);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return getDataAdapterList;

    }




    public List<Notifications> getAllNotifs(){
        String[] columns = {
                "id",
                MSG_BODY,
                MSG_TITLE

        };


        List<Notifications> notificationses = new ArrayList<Notifications>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NOTIFS,//Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Notifications notifications = new Notifications();
                notifications.setBody(cursor.getString(cursor.getColumnIndex(MSG_BODY)));
                notifications.setTitle(cursor.getString(cursor.getColumnIndex(MSG_TITLE)));
                notifications.setId(cursor.getInt(cursor.getColumnIndex("id")));

                // Adding user record to list
                notificationses.add(notifications);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return notificationses;

    }


	
	public void updateData(GetDataAdapter getDataAdapter) {
 
     final SQLiteDatabase db = this.getWritableDatabase();
 
     ContentValues values = new ContentValues();
     values.put(PROD_ID, getDataAdapter.getId());
     values.put(PROD_NAME, getDataAdapter.getName());
	 values.put(PROD_PRICE, getDataAdapter.getPrice());
     values.put(PROD_QTY, getDataAdapter.getQty());
     values.put(PROD_SIZE, getDataAdapter.getSize());
     db.update(TABLE_ORDERS,values,PROD_ID+"=? AND "+PROD_SIZE+"=?", new String[]{getDataAdapter.getId(),getDataAdapter.getSize() } );
	 db.close();
 }
 
 public List<GetDataAdapter> getRow(String id) {
 
     SQLiteDatabase db = this.getReadableDatabase();
	 
	 String[] columns = {
                PROD_ID,
                
                PROD_SIZE               
        };
		
		String sortOrder = PROD_NAME + " ASC";
		List<GetDataAdapter> getDataAdapterList = new ArrayList<GetDataAdapter>();

 
     Cursor cursor = db.query(TABLE_ORDERS, columns, PROD_ID + "=?",
             new String[] { id }, null, null, sortOrder);
 
 
    if (cursor.moveToFirst()) {
            do {
                GetDataAdapter getDataAdapter = new GetDataAdapter();
                getDataAdapter.setId(cursor.getString(cursor.getColumnIndex(PROD_ID)));
                getDataAdapter.setSize(cursor.getString(cursor.getColumnIndex(PROD_SIZE)));
              
                // Adding user record to list
                getDataAdapterList.add(getDataAdapter);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
     // return user data
     return getDataAdapterList;
 }

 public int getTotalAmount(){
     String[] columns = {
             PROD_PRICE,
             PROD_QTY,
     };

     SQLiteDatabase db = this.getReadableDatabase();

     Cursor cursor = db.query(TABLE_ORDERS, //Table to query
             columns,    //columns to return
             null,        //columns for the WHERE clause
             null,        //The values for the WHERE clause
             null,       //group the rows
             null,       //filter by row groups
             null); //The sort order

     int total = 0;

     if (cursor.moveToFirst()) {
         do {
             int price = Integer.parseInt(cursor.getString(cursor.getColumnIndex(PROD_PRICE)));
             int quant = Integer.parseInt(cursor.getString(cursor.getColumnIndex(PROD_QTY)));
             total += (price*quant);

         } while (cursor.moveToNext());
     }
     cursor.close();
     db.close();
     return total;
 }


    public boolean isInWishlist(String id) {

        SQLiteDatabase db = this.getReadableDatabase();

        String[] columns = {
                PROD_ID
        };

        boolean isPresent = false;

        Cursor cursor = db.query(TABLE_WISHLIST, columns, PROD_ID + "=?",
                new String[] { id }, null, null, null);


        if (cursor.moveToFirst()) {
            isPresent = true;
        }
        cursor.close();
        db.close();
        return isPresent;
    }




    public int cart_count(){
        String[] columns = {
                "uni_id"
        };

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_ORDERS, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order

        int total = 0;

        if (cursor.moveToFirst()) {
            do {
                total++;

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return total;
    }



}
