package com.android.closethunt;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ViewcartActivity extends AppCompatActivity {

    List<GetDataAdapter> GetDataAdapter1;
    RecyclerView recyclerView;
    RecyclerView.Adapter recyclerViewadapter;
    DBHelper dbHelper;
    AppCompatTextView total;
    AppCompatButton checkout;
    int count = 0, sum=0;
    DatabaseReference database;
    String getid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewcart);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        total = (AppCompatTextView) findViewById(R.id.inr);
        database = FirebaseDatabase.getInstance().getReference().child("Order_History").child(FirebaseAuth.getInstance().getCurrentUser().getUid().toString());

        dbHelper = new DBHelper(getApplicationContext());
        GetDataAdapter1 = dbHelper.getAllCartItems();
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView1);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        if (GetDataAdapter1.size() > 0) {
            recyclerViewadapter = new CartRecyclerViewAdapter(GetDataAdapter1, this, total);
            recyclerView.setAdapter(recyclerViewadapter);
            recyclerViewadapter.notifyDataSetChanged();
        } else {
			onBackPressed();
        Toast.makeText(getApplicationContext(),"Cart is Empty", Toast.LENGTH_LONG).show();}
            //recyclerView.setAdapter(null);
        if (recyclerViewadapter!= null)
            recyclerViewadapter.notifyDataSetChanged();

        for(int i=0; i<GetDataAdapter1.size(); i++){
            count += ((GetDataAdapter1.get(i).getQty())*(Integer.parseInt(GetDataAdapter1.get(i).getPrice())));
            total.setText("INR. "+ String.valueOf(count));
        }

        checkout = (AppCompatButton) findViewById(R.id.checkout);
        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ConnectivityManager conMgr =  (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null) {
                    Toast.makeText(getApplicationContext(), "No Network Connection", Toast.LENGTH_LONG).show();
                    return;
                }

                else if(netInfo.isConnected()){
                    if(count==0){
                    Toast.makeText(getApplicationContext(),"Please Add Items to Cart", Toast.LENGTH_LONG).show();
                } else{
                    getid = database.push().getKey();
                    Random rand = new Random();
                    String randomkey = String.valueOf(rand.nextInt(999999));
                    String status = "Dispatched";
                    for(int j=0; j<GetDataAdapter1.size(); j++){
                        database.child(randomkey).child("Products").child(GetDataAdapter1.get(j).getId()).child("name").setValue(GetDataAdapter1.get(j).getName());
                        database.child(randomkey).child("Products").child(GetDataAdapter1.get(j).getId()).child("qty").setValue(String.valueOf(GetDataAdapter1.get(j).getQty()));
                        database.child(randomkey).child("Products").child(GetDataAdapter1.get(j).getId()).child("color").setValue(GetDataAdapter1.get(j).getColor());
                        database.child(randomkey).child("Products").child(GetDataAdapter1.get(j).getId()).child("size").setValue(GetDataAdapter1.get(j).getSize());
                        database.child(randomkey).child("Products").child(GetDataAdapter1.get(j).getId()).child("price").setValue(GetDataAdapter1.get(j).getPrice());

                    }
                    database.child(randomkey).child("Amount").setValue(total.getText().toString());
                    database.child(randomkey).child("Id").setValue(randomkey);
                    database.child(randomkey).child("status").setValue(status);
                    dbHelper.emptyCart();

                    Intent i = new Intent(ViewcartActivity.this, CheckoutActivity.class);
                    i.putExtra("totalAmount", count);
                    startActivity(i);
                }}
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.cart_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id==R.id.cartmenu){
            startActivity(new Intent(ViewcartActivity.this, ProfileActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }
}
